const createUniverse = async (req, res) => {
    try {
        const universe = await Universe.create().fetch();

        return res.status(200).send({
            success: {
                message: "Successfully created a new universe",
                data: {
                    universeID: universe.id
                }
            }
        });

    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to create a universe"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
};

const fetchAll = async (req, res) => {
    try {
        const universeList = await Universe.find() || [];

        return res.status(200).send({
            success: {
                message: "Successfully fetch list of universes",
                data: {
                    universeList: universeList.map(x => x.id)
                }
            }
        });

    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to fetch a list of universes"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
}

module.exports = {
    createUniverse,
    fetchAll
};
