const Joi = require("@hapi/joi");
const BlueBird = require("bluebird");
Family.getDatastore().sendNativeQuery = BlueBird.promisify(Family.getDatastore().sendNativeQuery);

const createSchema = {
    body: Joi.object().keys({
        familyName: Joi.string().alphanum().required()
    })
};

const create = async (req, res) => {
    try {
        let validation = Joi.validate(req.body, createSchema.body);
        if (validation.error) {
            return res.status(400).send({
                error: {
                    message: validation.error.message
                }
            });
        }

        const familyName = req.body.familyName;

        try {
            await Family.create({ id: familyName });

            return res.status(200).send({
                success: {
                    message: `Successfully created a new family`
                }
            });
        } catch(err) {
            if (err.code === "E_UNIQUE") {
                return res.status(400).send({
                    error: {
                        message: "A family with this name already exists"
                    },
                    code: "UNIQUENESS_ERROR"
                });
            }
            throw err;
        }

    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to create the family"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
};

const fetchAllSchema = {
    query: Joi.object().keys({
        universeID: Joi.number().integer().min(1)
    })
};

const fetchAll = async (req, res) => {
    try {
        let validation = Joi.validate(req.query, fetchAllSchema.query);
        if (validation.error) {
            return res.status(400).send({
                error: {
                    message: validation.error.message
                }
            });
        }

        const universeID = req.query && req.query.universeID;
        let families;
        if (universeID) {
            const isValid = await UniverseService.doesExist(universeID);
            if (!isValid) {
                return res.status(400).send({
                    error: {
                        message: "Invalid universe ID"
                    },
                    code: "INVALID_UNIVERSE"
                });
            }

            families = await Family.getDatastore().sendNativeQuery(`
                SELECT DISTINCT family_id FROM person
                WHERE
                    universe_id=$1
            `, [universeID]);

            families = families.rows;
            families = families.map(x => x.family_id);
        } else {
            families = await Family.find();
        }

        return res.status(200).send({
            success: {
                message: `Successfully fetched families list`,
                data: {
                    families
                }
            }
        });

    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to fetch the families list"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
};

const isBalancedSchema = {
    query: Joi.object().keys({
        familyName: Joi.string().alphanum().required()
    })
};

const isBalanced = async (req, res) => {
    try {
        let validation = Joi.validate(req.query, isBalancedSchema.query);
        if (validation.error) {
            return res.status(400).send({
                error: {
                    message: validation.error.message
                }
            });
        }

        const familyName = req.query.familyName;
        const isFamilyValid = await FamilyService.doesExist(familyName);

        if (!isFamilyValid) {
            res.status(400).send({
                error: {
                    message: "Invalid family name"
                },
                code: "INVALID_FAMILY"
            });
        }

        let isBalanced = true;
        let families = await Family.getDatastore().sendNativeQuery(
            "SELECT DISTINCT family_id, power FROM person " +
            "WHERE family_id = $1 " +
            "LIMIT 2" , [familyName]);
        families = families.rows;

        if (families.length === 2) {
            isBalanced = false;
        }

        return res.status(200).send({
            success: {
                message: "Family found to be " + isBalanced ? "balanced": "unbalanced",
                data: {
                    isBalanced
                }
            }
        });

    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to determine if family is balanced or not"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
};

const createBalanceSchema = {
    body: {
        familyName: Joi.string().alphanum().required()
    }
};

const createBalance = async (req, res) => {
    try {
        let validation = Joi.validate(req.body, createBalanceSchema.body);
        if (validation.error) {
            return res.status(400).send({
                error: {
                    message: validation.error.message
                }
            });
        }

        const familyName = req.body.familyName;
        const isFamilyValid = await FamilyService.doesExist(familyName);

        if (!isFamilyValid) {
            return res.status(400).send({
                error: {
                    message: "Invalid family name"
                },
                code: "INVALID_FAMILY"
            });
        }

        await Person.update({ family_id: familyName }).set({ power: 0 });

        return res.status(200).send({
            success: {
                message: "Family is balanced across all the universes",
            }
        });

    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to balance out the family"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
};

const fetchAllUnbalanced = async (req, res) => {
    try {
        let unbalancedFamilies = await Family.getDatastore().sendNativeQuery(`
            SELECT R.family_id FROM 
            (
                SELECT DISTINCT family_id, power FROM person
                WHERE family_id IN (
                    SELECT id AS family_id FROM family
                )
            ) AS R
            GROUP BY R.family_id HAVING count(*) > 1`, []);
        unbalancedFamilies = unbalancedFamilies.rows;

        return res.status(200).send({
            success: "Successfully fetch all unbalanced families",
            data: {
                families: unbalancedFamilies.map(x => x.family_id)
            }
        });
    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to fetch list of unbalanced families"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
} 

module.exports = {
    create,
    fetchAll,
    isBalanced,
    createBalance,
    fetchAllUnbalanced
};
