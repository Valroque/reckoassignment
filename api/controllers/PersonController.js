const Joi = require("@hapi/joi");

const createSchema = {
    body: Joi.object().keys({
        personName: Joi.string().alphanum().required(),
        power: Joi.number().integer().required(),
        familyName: Joi.string().alphanum().required(),
        universeID: Joi.number().integer().min(1).required()
    })
};

const create = async (req, res) => {
    try {
        let validation = Joi.validate(req.body, createSchema.body);
        if (validation.error) {
            return res.status(400).send({
                error: {
                    message: validation.error.message
                }
            });
        }

        const id = req.body.personName;
        const power = req.body.power;
        const familyName = req.body.familyName;
        const universeID = req.body.universeID;

        try {
            await Person.create({ id, power, family_id: familyName, universe_id: universeID });
        } catch(err) {
            if (err.code === "E_UNIQUE") {
                return res.status(400).send({
                    error: {
                        message: "A person with this name already exists"
                    },
                    code: "UNIQUENESS_ERROR"
                });
            }

            if (err.raw.code === "ER_NO_REFERENCED_ROW_2") {
                return res.status(400).send({
                    error: {
                        message: "Failed to create the pereson. Invlid familyName/universeID"
                    },
                    code: "INVALID_INPUT"
                });
            }

            throw err;
        }

        return res.status(200).send({
            success: {
                message: "Successfully create the Person"
            }
        });

    } catch(err) {
        sails.log.error(err);
        return res.status(500).send({
            error: {
                message: "Failed to create the Person"
            },
            code: "INTERNAL_SERVER_ERROR"
        });
    }
};

module.exports = {
    create
};
