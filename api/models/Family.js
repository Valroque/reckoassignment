module.exports = {
    tableName: 'family',
    attributes: {
        id: {
            type: 'string',
            required: true
        }
    }
};
