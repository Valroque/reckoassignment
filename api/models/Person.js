module.exports = {
    tableName: 'person',
    attributes: {
        id: {
            type: 'string',
            required: true
        },
        power: {
            type: 'number',
            required: true
        },
        family_id: {
            type: 'string',
            required: true
        },
        universe_id: {
            type: 'number',
            required: true
        }
    }
};
