module.exports = {
    doesExist: async (familyName) => {
        let isValid = true;

        if (!familyName) {
            isValid = false;
        }

        const family = await Family.findOne({ id: familyName });

        if (!family) {
            isValid = false;
        }

        return isValid;
    }
};
