module.exports = {
    doesExist: async (universe) => {
        let isValid = true;

        if (!universe) {
            isValid = false;
        }

        const universeObject = await Universe.findOne({ id: universe });

        if (!universeObject) {
            isValid = false;
        }

        return isValid;
    }
};
