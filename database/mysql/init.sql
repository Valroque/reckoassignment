-- Initializes up all the needed databases and tables within to get the app running
-- ## If db already exists all data will be lost

-- Create the database
DROP DATABASE IF EXISTS recko;
CREATE DATABASE recko;

-- Select the database
USE recko;

-- Initialize tables
CREATE TABLE universe (
    `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `createdAt` TIMESTAMP NULL DEFAULT NULL,
    `updatedAt` TIMESTAMP NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- "family" table - contains records of all distinct families in all the universes
CREATE TABLE family (
    `id` VARCHAR(100) NOT NULL PRIMARY KEY,
    `createdAt` TIMESTAMP NULL DEFAULT NULL,
    `updatedAt` TIMESTAMP NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- "person" table - contains records of all persons in all the universes
CREATE TABLE person (
   `id` VARCHAR(255) NOT NULL PRIMARY KEY,
   `power` INT NOT NULL,
   `family_id` VARCHAR(100) NOT NULL,
   `universe_id` INT NOT NULL,
   `createdAt` TIMESTAMP NULL DEFAULT NULL,
   `updatedAt` TIMESTAMP NULL DEFAULT NULL,
   CONSTRAINT person_fk1 FOREIGN KEY(family_id) REFERENCES family(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
CREATE INDEX person_idx1 ON person(family_id, universe_id);
ALTER TABLE person ADD CONSTRAINT person_fk2 FOREIGN KEY (universe_id) REFERENCES universe(id);