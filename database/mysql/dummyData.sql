USE recko

INSERT INTO universe (id) VALUES (1), (2), (3), (4), (5), (6);

INSERT INTO family (id) VALUES ("F1"), ("F2"), ("F3"), ("F4"), ("F5"), ("F6");

INSERT INTO person (id, family_id, universe_id, power) VALUES 
    ("P1", "F1", 1, -4),
    ("P2", "F1", 2, -2),
    ("P3", "F1", 3, 0),
    ("P4", "F1", 4, 2),
    ("P5", "F1", 5, 4),
    ("P6", "F1", 6, 6),
    ("P7", "F2", 1, -2),
    ("P8", "F2", 3, -2),
    ("P9", "F2", 5, -2),
    ("P10", "F2", 1, -2),
    ("P11", "F2", 3, -2),
    ("P12", "F2", 5, -2),
    ("P13", "F3", 1, 0),
    ("P14", "F3", 1, 0),
    ("P15", "F3", 1, 0),
    ("P16", "F3", 1, 0),
    ("P17", "F3", 1, 0),
    ("P18", "F3", 1, 0);
