# recko-assignment

[Postman Collection for APIS](https://www.getpostman.com/collections/d11161ae07e9b4779930)


1. DB - MySQL.
2. server - NodeJS (sails.js framework)
3. ORM - Waterline


# Installation
pre-req: Mysql server

1. _clone the repo_ and install the dependencies

	* `git clone https://bitbucket.org/Valroque/reckoassignment/src/master/ "forderName"`
	* `cd "folderName"`
	* `npm install`

2. _input the mysql server details in ./private/db.js

3. startTheApp:

	* `node app.js`

### The app is accessible on "localhost:1337". API.md can be referred to under stand API usage.