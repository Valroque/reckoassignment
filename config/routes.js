/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
    // Universe APIS
    'POST /universe/create':                'UniverseController.create',
    'GET /universe/fetchAll':               'UniverseController.fetchAll',

    // Family APIS
    'POST /family/create':                  'FamilyController.create',
    'GET /family/fetchAll':                 'FamilyController.fetchAll',
    'GET /family/isBalanced':               'FamilyController.isBalanced',
    'PUT /family/createBalance':            'FamilyController.createBalance',
    'GET /family/fetchAllUnbalanced':       'FamilyController.fetchAllUnbalanced',

    // Person APIS
    'POST /person/create':                  'PersonController.create',
};
