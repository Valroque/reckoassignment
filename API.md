# API DOCUMENTATION #

The app has 3 modules which the APIs help to interact with:

* Universe
* Family
* Person

### Universe ###

create: POST `/universe/create` Create a universe

```
1. body: none
2. query: none
3. params: none
```

### Family ###

_create_: POST `/family/create` Create a family

```
1. body:
{ 
	familyName: String - Alphanumeric
}
2. query: none
3. params: none
```

---
_fetchAll_: GET `/family/fetchAll?universeId=1` Fetch all, already created, families

```
1. body: none
2. query:
	{
		[universeId]: Positive Integer // if provided, fetch all family_id's in a universe
	}
3. params: none
```

---
_isBalanced_: GET `/family/isBalanced` Check if a family is balanced or not

```
1. body: none
2. query:
	{
		familyName: String - Alphanumeric
	}
3. params: none
```

---
_createBalance_: PUT `/family/createBalance` Balance out a family

```
1. body:
	{
		familyName: String - Alphanumeric
	}
2. query: none
3. params: none
```

---
_fetchAllUnbalanced_: GET `/family/fetchAllUnbalanced` Fetch all unbalanced familes

```
1. body: none
2. query: none
3. params: none
```

### Person ###

_create_: POST `/person/create` Create a person

```
1. body: 
	{
		personName: String - Alphanumeric
		power: Integer
		familyName: String - Alphanumeric
		universeID: Positive Integer
	}
2. query: none
3. params: none
```
